const path = require("path");
const htmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require("webpack");
module.exports = {
  entry: {
    main: "./src/index.js",
  },
  resolve: {
    extensions: [".js", ".jsx"], // 引入其它模块时且不写文件后缀时避免报错，会尝试找.js和.jsx文件
    alias: {
      header: path.resolve(__dirname, "../src/Header.js"),
    },
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/, // js和jsx文件
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          // options: {  // 在babel.config.json文件中配置
          //   presets: ['@babel/preset-env']
          // }
        },
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            name: "[name]_[hash:6].[ext]",
            outputPath: "images/",
            limit: 10240, // 10kb
          },
        },
      },
      {
        test: /\.(eot|svg|ttf|woff)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name]_[hash:6].[ext]",
            outputPath: "fonts/",
          },
        },
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader", // 将 JS 字符串生成为 style 节点
          // importLoaders: scss文件里面的@import 引入的文件也需要走sass-loader, postcss-loader这两个loader，否则直接css-loader和style-loader
          {
            loader: "css-loader", // 将 CSS 转化成 CommonJS 模块
            options: {
              importLoaders: 2,
              // modules: true,  // 开启css模块化
            },
          },
          "postcss-loader",
          "sass-loader", // 将 Sass 编译成 CSS
        ],
      },
    ],
  },
  output: {
    // 相当于cd __dirname（表示当前目录：F:\project\webpack-demo\build\dist ）,
    path: path.resolve(__dirname, "../dist"),
    filename: "[name].js", // 对应入口文件
    chunkFilename: "[name].chunk.js", // 对应其他文件
  },
  plugins: [
    new CleanWebpackPlugin(),
    // 第一步以template为模板生成html文件，第二步把output打包的js文件注入到html中
    new htmlWebpackPlugin({
      template: "./public/index.html",
    }),
    // new webpack.HotModuleReplacementPlugin(),  // 如果启动命令用的--hot，会自动添加,不需要配置
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    // })
  ],
  optimization: {
    // runtimeChunk: {
    // 	name: 'runtime'
    // },
    splitChunks: {
      chunks: "all",
      /*    cacheGroups: {
      	vendors: {
      		test: /[\\/]node_modules[\\/]/,
      		priority: -10,
      		name: 'vendors',
      	}
      } */
    },
    usedExports: true, // 只导入使用到的
  },
};
