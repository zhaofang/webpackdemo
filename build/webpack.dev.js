const path = require("path");
const {merge} = require('webpack-merge');
const commonConfig = require('./webpack.common.js');
const devConfig = {
  mode: "development",
  // 可以知道报错的代码：dist/bundle.js中报错的行对应于src中源js文件的多少行， 一般development才配置
  // devtool: 'source-map',  // source-map 生成.map文件，包含映射关系
  // devtool: 'inline-source-map',  // inline-source-map不生成.map文件，把sourceMap打包成base64的字符串放进bundle.js最后
  devtool: "eval-cheap-module-source-map", // development 最推荐, cheap只提示多少行出错，不提示多少列出错，提高构建速度,module报错包含导入的三方库文件，而不只是业务代码
  devServer: {
    contentBase: path.resolve(__dirname, "../dist"),  // 对应目录下启动服务器
    open: true,
   // hot: true,
    hotOnly: true, // HMR没有生效，浏览器也不自动刷新
  }
};
module.exports = merge(commonConfig, devConfig);
