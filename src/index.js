// import "@babel/polyfill";    // useBuiltIns = usage 不需要再引入，会自动引入
import React, { Component } from "react";
import ReactDom from "react-dom";
// import _ from 'lodash';
import Header from 'header';
// import Content from './Content'
// import Footer from './Footer'
// import Avatar from './Avatar'
import './index.scss';
import "./iconfont.scss"; // HMR 样式文件修改，页面会自动刷新，因为sass-loader内部自己实现了module.hot
import { add } from "./math";
/* const render = () => {
    Header()
    Avatar()
    Content()
    Footer()
}
render()
if(module.hot) {
    module.hot.accept('./Header.js', () => {
        document.getElementById('root').innerHTML = null
        render()
    })
} */
class App extends Component {
  getComponent() {
    // 魔术注释：webpackChunkName 打包之后的文件名
    return import(/* webpackChunkName: "lodash" */ "lodash").then(
      ({ default: _ }) => {
        var element = document.createElement("div");
        element.innerHTML = _.join(["Hello", "World"], "~");
        return element;
      }
    );
  }

  componentDidMount() {
    this.getComponent().then((element) => {
      document.body.appendChild(element);
    });
    Header()
  }

  render() {
    return (
      <div>
        <span className="iconfont iconlisten"></span>
        计算{add(1, 5)}
        {/* {_.join(["a", "b", "c"], "####")} */}
      </div>
    );
  }
}
ReactDom.render(<App />, document.getElementById("root"));
